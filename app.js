//[SECTION] Packages and Dependencies
const express = require("express"); 
const mongoose = require("mongoose");  
const dotenv = require("dotenv");  
const courseRoutes = require('./routes/courses'); 
const userRoutes = require('./routes/users');
const cors = require("cors");


//[SECTION] Server Setup
const app = express(); 
dotenv.config(); 
app.use(express.json());
app.use(cors())
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT; 

//[SECTION] Application Routes

app.use('/courses', courseRoutes);
app.use('/users', userRoutes);

//[SECTION] Database Connection
mongoose.connect(secret)
let connectStatus = mongoose.connection; 
connectStatus.on('open', () => console.log('Database is Connected'));

//[SECTION] Gateway Response
app.get('/', (req, res) => {
   res.send(`Welcome to my Course Booking APP`); 
}); 
app.listen(port, () => console.log(`Server is running on port ${port}`)); 
