//[SECTION] Dependencies and Modules
const res = require('express/lib/response');
const Course = require('../models/Course');

//[SECTION] Functionality [CREATE]
   module.exports.createCourse = (info) => {
     let cName = info.name;
     let cDesc = info.description;
     let cCost = info.price;
     let sActive = info.isActive;

     let newCourse = new Course({
     	name: cName,
     	description: cDesc,
     	price: cCost,
      isActive: sActive
     }) 
   	 return newCourse.save().then((savedCourse, error) => {
   	 	if (error) {
   	 		return res.send(false);
   	 	} else {
   	 		return savedCourse; 
   	 	}
   	 });
   };

//[SECTION] Functionality [RETRIEVE]
   
   module.exports.getAllCourse = () => {
     return Course.find({}).then(outcomeNiFind => {
        return outcomeNiFind;
     });
   };
  
   module.exports.getCourse = (id) => {
      
      return Course.findById(id).then(resultOfQuery => {
         return resultOfQuery;
      });
   };
  
   module.exports.getAllActiveCourses = () => {
   return Course.find({isActive: true}).then(resultOfTheQuery => {
       return resultOfTheQuery;
   });
   }
//[SECTION] Functionality [UPDATE]
   
   module.exports.updateCourse = (id, details) => {
         
         let cName = details.name;
         let cDesc = details.description;       
         let cCost = details.price;
         
         let updateCourse = {
            name: cName,
            description: cDesc,
            price: cCost
         }
         
         return Course.findByIdAndUpdate(id, updateCourse).then((courseUpdated, error) => {
           
            if (error) {
               return false;
            } else {
               return true;
            };
         });
         
   }

   module.exports.deactivateCourse = (id) => {
     
      let updates = {
         isActive:false
      }
      
    return Course.findByIdAndUpdate(id, updates).then((archived, error) => {
       
      if (archived) {
        return `The Course ${id} has been deactivated`;
      } else {
        return 'Failed to archive course'; 
      };
   });
   };

   module.exports.reactivateCourse = (id) => {
     
      let updates = {
         isActive:true
      }
      
    return Course.findByIdAndUpdate(id, updates).then((reactivate, error) => {
       
      if (reactivate) {
        return `The Course ${id} has been activated`;
      } else {
        return 'Failed to activate course'; 
      };
   });
   };


//[SECTION] Functionality [DELETE]
   module.exports.deleteCourse = (id) => {
     
      return Course.findByIdAndRemove(id).then((removedCourse, err) => {
        
         if (removedCourse){
            return 'Course Succesfully Deleted'
         } else {
            return 'No Course Was Removed'
         }
      });
   }

