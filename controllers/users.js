//[SECTION] Dependencies and Modules
  const User = require('../models/User');
  const bcrypt = require ("bcrypt");
  const dotenv = require ("dotenv");
  const auth = require("../auth")
  const Course = require('../models/Course');

//[SECTION] Environment Setup
  dotenv.config();
  const salt = parseInt(process.env.Salt);
  
//[SECTION] Functionality [CREATE]
   module.exports.createUser = (info) => {
   	 
     let fName = info.firstName;
     let lName = info.lastName;
     let Useremail = info.email;
     let Userpassword = info.password;
     let Usergender =info.gender;
     let mobileNum =info.mobileNo;

     let newUser = new User({
     	 firstName: fName,
     	 lastName: lName,
     	 email: Useremail,
         password: bcrypt.hashSync(Userpassword, salt),
         gender: Usergender,
         mobileNo: mobileNum

     }) 

     return User.findOne({email: Useremail}).then((user) => {

      if(user){
          return {message: `User already exists`}
      }else {
          return newUser.save().then((savedUser, error) => {
         
              if (error) {
                   
                  return {message: 'Failed to Save New User'};
              } else {
                  
                  return savedUser; 
              }
          });
      }
  });
};



module.exports.loginUser = (req, res) => {

    console.log(req.body)

    User.findOne({email: req.body.email})
    .then(foundUser => {

      if(foundUser === null){
        return res.send({ message: "User Not Found"});

      } else {

        const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

        console.log(isPasswordCorrect)
        
        if(isPasswordCorrect){

          return res.send({accessToken: auth.createAccessToken(foundUser)})

        } else {

          return res.send(false)
        }

      }

    })
    .catch(err => res.send(err))

};

//[SECTION] REtrieve
module.exports.getAllUser = () => {
  return User.find({}).then(result => {
    return result;
  });
}


//!Get User Details

module.exports.getUserDetails = (req, res) => {

    console.log(req.user)

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))
};


//[SECTION] Functionality [RETRIEVE]
    module.exports.getAllUsers = () => {
        return User.find({}).then(outcomeNiFind => {
        return outcomeNiFind;
        });
    };
    
    module.exports.getUser = (id) => {
        
        return User.findById(id).then(resultOfQuery => {
            return resultOfQuery;
        });
    };
    
    module.exports.getAllActiveUsers = () => {
    return User.find({isActive: true}).then(resultOfTheQuery => {
        return resultOfTheQuery;
    });
    }


//!ENrollments

  module.exports.enroll = async (req, res) => {


    console.log(req.user.id) 
    console.log(req.body.courseId) 


    if(req.user.isAdmin){
      return res.send("Action Forbidden")
    }


    let isUserUpdated = await User.findById(req.user.id).then(user => {

      let newEnrollment = {
          courseId: req.body.courseId
      }

      user.enrollments.push(newEnrollment);

      return user.save().then(user => true).catch(err => err.message)


    })

    
    if(isUserUpdated !== true) {
        return res.send({message: isUserUpdated})
    }

    let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {


      let enrollee = {
          userId: req.user.id
      }

      course.enrollees.push(enrollee);

      return course.save().then(course => true).catch(err => err.message)


    })

    if(isCourseUpdated !== true) {
        return res.send({ message: isCourseUpdated})
    }


    if(isUserUpdated && isCourseUpdated) {
        return res.send({ message: "Enrolled Successfully."})
    }


  }


  module.exports.getEnrollments = (req, res) => {
      User.findById(req.user.id).then(result => res.send(result.enrollments))
      .catch(err => res.send(err))
  }

//[SECTION] Functionality [UPDATE]

//[SECTION] Functionality [DELETE]
  module.exports.deleteUser = (id) => {
     
      return User.findByIdAndRemove(id).then((removedUser, err) => {
        
         if (removedUser){
            return 'User Succesfully Deleted'
         } else {
            return 'No User Was Removed'
         }
      });
   }




