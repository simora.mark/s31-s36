//[SECTION] Dependencies and Modules
  
const exp = require("express");
const controller = require('../controllers/users');
const auth = require("../auth")


const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component

const route = exp.Router(); 

//[SECTION] [POST] Routes
  route.post('/register', (req, res) => {
    let data = req.body; 
    controller.createUser(data).then(outcome => {
            res.send(outcome); 
    });
  });


//[SECTION] RETRIEVE

route.get('/all', (req, res) => {
    
  controller.getAllUser().then(outcome => {
    // console.log(outcome)
    res.send(outcome);
  });
});

  // !LOGIN
  
  route.post("/login", controller.loginUser);

//! GET USER DETAILS
  
  route.get("/getUserDetails", verify, controller.getUserDetails)

  //[SECTION] [GET] Routes

  route.get('/all', verify, verifyAdmin, (req, res) => {
    controller.getAllUsers().then(outcome => {
    res.send(outcome);
    });
  });

  route.get('/:id', (req, res) => {
    let data = req.params.id;
        controller.getUser(data).then(outcome => {
            res.send(outcome);
        });
  });

  route.get('/', (req, res) => {
    controller.getAllActiveUsers().then(outcome => {
        res.send(outcome);
    });
  });

//!ENROLL OUR REGISTERED USER
route.post('/enroll', verify, controller.enroll)




//!GET LOGGED USER'S ENROLLMENTS
route.get('/getEnrollments', verify, controller.getEnrollments)


//[SECTION] [PUT] Routes

//[SECTION] [DEL] Routes
  route.delete('/:id', (req, res) => {
    let id = req.params.id;
        controller.deleteUser(id).then(outcome => {
          res.send(outcome);
        })
  });

//[SECTION] Export Route System

module.exports = route; 
