//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/courses');
const auth = require("../auth")

const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router(); 

//[SECTION] [POST] Routes
    route.post('/create', (req, res) => { 
        let data = req.body; 
        controller.createCourse(data).then(outcome => {
              res.send(outcome); 
        });
    });

//[SECTION] [GET] Routes
    route.get('/all', (req, res) => {
      
        controller.getAllCourse().then(outcome => {
          console.log(outcome)
          res.send(outcome);
        });
    });

    route.get('/:id', (req, res) => {
      
      let data = req.params.id;
      controller.getCourse(data).then(outcome => {
        res.send(outcome);
      }); 
    });

    route.get('/', (req, res) => {
      controller.getAllActiveCourses().then(outcome => {
          res.send(outcome);
      });
    });

//[SECTION] [PUT] Routes
route.put('/:id', verify, verifyAdmin,(req, res) => {
  let id = req.params.id; 
  let details = req.body;
  let cName = details.name; 
  let cDesc = details.description;
  let cCost = details.price;       
  if (cName  !== '' && cDesc !== '' && cCost !== '') {
    controller.updateCourse(id, details).then(outcome => {
        res.send(outcome); 
    });      
  } else {
    res.send({message: 'Incorrect Input, Make sure details are complete'});
  }
}); 


route.put('/:id/archive', (req, res) => {
  
  let courseId = req.params.id; 

  controller.deactivateCourse(courseId).then(resultOfTheFunction => {
      
      res.send(resultOfTheFunction);
  });
});

route.put('/:id/reactivate', (req, res) => {
  
  let courseId = req.params.id; 

  controller.reactivateCourse(courseId).then(resultOfTheFunction => {
      
      res.send(resultOfTheFunction);
  });
});

//[SECTION] [DEL] Routes

      route.delete('/:id', (req, res) => {
        let id = req.params.id;
        controller.deleteCourse(id).then(outcome => {
          res.send(outcome);
        })

      });

//[SECTION] Export Route System
module.exports = route; 
